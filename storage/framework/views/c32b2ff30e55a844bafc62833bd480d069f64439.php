<!DOCTYPE html>
<html>

<?php echo $__env->make("includes.header", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<body class="fixed-navigation">

   <div id="loading" >
     <div class="sk-spinner sk-spinner-rotating-plane loader" ></div>
   </div>
                                       
 
    <div id="wrapper" class="animated fadeInRight">
    
        <?php echo $__env->make("includes.sidebar", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <div id="page-wrapper" class="gray-bg ">
        <div class="row border-bottom">
        
            <?php echo $__env->make("includes.header_nav", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2><?php echo e(isset($sub_breadcrumb)?$sub_breadcrumb:$breadcrumb); ?></h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo e(url('/home')); ?>">Home</a>
                        </li>
                        
                        <li <?php if(isset($sub_breadcrumb)): ?> class="" <?php else: ?> class="active" <?php endif; ?>>
                        <?php if(!isset($sub_breadcrumb)): ?> <strong> <?php endif; ?> <?php echo e($breadcrumb); ?> <?php if(!isset($sub_breadcrumb)): ?> </strong> <?php endif; ?>
                        </li>

                        <li <?php if(isset($sub_breadcrumb)): ?> class="active" <?php endif; ?>>
                        <?php if(isset($sub_breadcrumb)): ?> <strong> <?php endif; ?> <?php echo e(isset($sub_breadcrumb)?$sub_breadcrumb:""); ?> <?php if(isset($sub_breadcrumb)): ?> </strong> <?php endif; ?>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>

            <?php echo $__env->make('includes.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php echo $__env->yieldContent('content'); ?>

                <div class="footer">
                    <div class="row">
                    <div class="col-lg-12">
                    
                    <div class="col-md-3" style="display: inline-block;font-size:11px">
                        <strong>Copyright</strong> AdMe Marketing Services &copy; 2018
                    </div>
                     
                                <div class="col-md-3" style="display: inline-block;">
                                    <div style="text-align: center" >
                                        <img src="<?php echo e(asset('images/ph.png')); ?>" alt="philippine time" width="20px" height="15px">
                                        <span>Philippine </span>
                                        
                                        <label class="label label-primary" style="margin-top:5px;font-size:12px"><i class="fa fa-clock-o"></i> <span id="clock_ph"> </span></label>
                                    </div>
                                </div>
                                <div class="col-md-3" style="display: inline-block">
                                    <div style="text-align: center" >
                                        <img src="<?php echo e(asset('images/usa.png')); ?>" alt="philippine time" width="20px" height="15px">
                                        <span>California </span>
                                        
                                        <label class="label label-danger" style="margin-top:2px;font-size:12px"><i class="fa fa-clock-o"></i> <span id="clock_ca"></span> </label>
                                    </div>
                                </div>
                                <div class="col-md-3" style="display: inline-block">
                                    <div style="text-align: center" >
                                            <img src="<?php echo e(asset('images/aus.png')); ?>" alt="philippine time" width="20px" height="15px">
                                            <span>Australia 
                                            <label class="label label-success" style="margin-top:2px;font-size:12px"><i class="fa fa-clock-o"> </i> <span id="clock_aus"></span>  </label>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>

              </div>
        
        </div>
    </div>
   
   <?php echo $__env->make("includes.footer", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
    
   <?php echo $__env->yieldContent("custom_js"); ?>
</body>

</html>

