   <!-- Mainly scripts -->
   
   <script src="http://code.jquery.com/jquery-latest.js"></script>
   <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script> -->
    
    <script src="<?php echo e(asset('inspinia_admin/js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('inspinia_admin/js/plugins/metisMenu/jquery.metisMenu.js')); ?>"></script>
    <script src="<?php echo e(asset('inspinia_admin/js/plugins/slimscroll/jquery.slimscroll.min.js')); ?>"></script>
    
    
    <!-- Flot -->
    <script src="<?php echo e(asset('inspinia_admin/js/plugins/flot/jquery.flot.js')); ?>"></script>
    <script src="<?php echo e(asset('inspinia_admin/js/plugins/flot/jquery.flot.tooltip.min.js')); ?>"></script>
    <script src="<?php echo e(asset('inspinia_admin/js/plugins/flot/jquery.flot.spline.js')); ?>"></script>
    <script src="<?php echo e(asset('inspinia_admin/js/plugins/flot/jquery.flot.resize.js')); ?>"></script>
    <script src="<?php echo e(asset('inspinia_admin/js/plugins/flot/jquery.flot.pie.js')); ?>"></script>
    <script src="<?php echo e(asset('inspinia_admin/js/plugins/flot/jquery.flot.symbol.js')); ?>"></script>
    <script src="<?php echo e(asset('inspinia_admin/js/plugins/flot/curvedLines.js')); ?>"></script>

    <!-- Peity -->
    <script src="<?php echo e(asset('inspinia_admin/js/plugins/peity/jquery.peity.min.js')); ?>"></script>
    <script src="<?php echo e(asset('inspinia_admin/js/demo/peity-demo.js')); ?>"></script>

    <!-- Custom and plugin javascript -->   
    <script src="<?php echo e(asset('inspinia_admin/js/inspinia.js')); ?>"></script>
    <script src="<?php echo e(asset('inspinia_admin/js/plugins/pace/pace.min.js')); ?>"></script>
  <!-- DROPZONE -->
  <script src="<?php echo e(asset('inspinia_admin/js/plugins/dropzone/dropzone.js')); ?>"></script>
    <!-- jQuery UI -->
    <script src="<?php echo e(asset('inspinia_admin/js/plugins/jquery-ui/jquery-ui.min.js')); ?>"></script>
    <!--Datatables -->
    <script src="<?php echo e(asset('inspinia_admin/js/plugins/dataTables/datatables.min.js')); ?>"></script>
    
    <!-- Jvectormap -->
    <script src="<?php echo e(asset('inspinia_admin/js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js')); ?>"></script>
    <script src="<?php echo e(asset('inspinia_admin/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')); ?>"></script>

    <!-- Sparkline -->
    <script src="<?php echo e(asset('inspinia_admin/js/plugins/sparkline/jquery.sparkline.min.js')); ?>"></script>

    <!-- Sparkline demo data  -->
    

    <!-- ChartJS-->
    <script src="<?php echo e(asset('inspinia_admin/js/plugins/chartJs/Chart.min.js')); ?>"></script>

   <!-- Data picker -->
   <script src="<?php echo e(asset('inspinia_admin/js/plugins/datapicker/bootstrap-datepicker.js')); ?>"></script>
    <!-- Ladda -->
    <script src="<?php echo e(asset('inspinia_admin/js/plugins/ladda/spin.min.js')); ?>"></script>
    <script src="<?php echo e(asset('inspinia_admin/js/plugins/ladda/ladda.min.js')); ?>"></script>
    <script src="<?php echo e(asset('inspinia_admin/js/plugins/ladda/ladda.jquery.min.js')); ?>"></script>

      <!-- Input Mask-->
      <script src="<?php echo e(asset('inspinia_admin/js/plugins/jasny/jasny-bootstrap.min.js')); ?>"></script>
    
     <!-- Tinycon -->
     <script src="<?php echo e(asset('inspinia_admin/js/plugins/tinycon/tinycon.min.js')); ?>"></script>

      <!-- Sweet alert -->
    <script src="<?php echo e(asset('inspinia_admin/js/plugins/sweetalert/sweetalert.min.js')); ?>"></script>

     <!-- iCheck -->
     <script src="<?php echo e(asset('inspinia_admin/js/plugins/iCheck/icheck.min.js')); ?>"></script>    
     <script src="<?php echo e(asset('js/timeAgo.min.js')); ?>"></script>

     <script src="<?php echo e(asset('vendor/upload-progress-bar/js/upload.js')); ?>"></script>
     <script src="<?php echo e(asset('vendor/cleave/cleave.min.js')); ?>"></script>
     <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>   
     <script src="https://craig.global.ssl.fastly.net/js/mousetrap/mousetrap.min.js?a4098"></script>
     
     <script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
     <script src="https://cdn.jsdelivr.net/npm/moment@2.24.0/moment.min.js"></script>
      <script src="<?php echo e(asset('js/moment-timezone-with-data.min.js')); ?>"> </script>
     <script src="<?php echo e(asset('js/custom.js')); ?>"></script>   
     
     <script src="<?php echo e(asset('vendor/number/jquery.number.js')); ?>"></script>
  
    
     <script src="http://malsup.github.com/jquery.form.js"></script> 
     <script>
            $(document).ready(function () {
               
                $("time.timeago").timeago();
               
                $('input[type=checkbox]').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });
                
                
            $('#quota').number( true, 2 );
       
                
            });
    </script>
     
    <!-- Custom Js -->
   
    <script type="text/javascript">
        $(window).on('load', function () {                
                $("#loading").hide("fade");
                
        });
    </script>

    <script >
         const clock_ph = document.getElementById('clock_ph');
         const clock_ca = document.getElementById('clock_ca');
         const clock_aus = document.getElementById('clock_aus');

         function updateTime(){
           const now_ph = moment().tz("Asia/Manila");
           const now_ca = moment().tz("America/Los_Angeles");
           const now_aus = moment().tz("Australia/Sydney");
            
            const humanReadable_ph = now_ph.format('hh:mm:ss A');
            const humanReadable_ca = now_ca.format('hh:mm:ss A');
            const humanReadable_aus = now_aus.format('hh:mm:ss A');

           clock_ph.textContent = humanReadable_ph;
           clock_ca.textContent = humanReadable_ca;
           clock_aus.textContent = humanReadable_aus;

           console.log(humanReadable);
         }

         setInterval(updateTime,1000);
         updateTime();
    </script>
    

    
  
   
   