<!DOCTYPE html>
<html>

@include("includes.header")

<body class="fixed-navigation">

   <div id="loading" >
     <div class="sk-spinner sk-spinner-rotating-plane loader" ></div>
   </div>
                                       
 
    <div id="wrapper" class="animated fadeInRight">
    
        @include("includes.sidebar")

        <div id="page-wrapper" class="gray-bg ">
        <div class="row border-bottom">
        
            @include("includes.header_nav")
            </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>{{ isset($sub_breadcrumb)?$sub_breadcrumb:$breadcrumb }}</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ url('/home') }}">Home</a>
                        </li>
                        
                        <li @if(isset($sub_breadcrumb)) class="" @else class="active" @endif>
                        @if(!isset($sub_breadcrumb)) <strong> @endif {{ $breadcrumb }} @if(!isset($sub_breadcrumb)) </strong> @endif
                        </li>

                        <li @if(isset($sub_breadcrumb)) class="active" @endif>
                        @if(isset($sub_breadcrumb)) <strong> @endif {{ isset($sub_breadcrumb)?$sub_breadcrumb:"" }} @if(isset($sub_breadcrumb)) </strong> @endif
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>

            @include('includes.alert')

            @yield('content')

                <div class="footer">
                    <div class="row">
                    <div class="col-lg-12">
                    
                    <div class="col-md-3" style="display: inline-block;font-size:11px">
                        <strong>Copyright</strong> AdMe Marketing Services &copy; 2018
                    </div>
                     
                                <div class="col-md-3" style="display: inline-block;">
                                    <div style="text-align: center" >
                                        <img src="{{ asset('images/ph.png') }}" alt="philippine time" width="20px" height="15px">
                                        <span>Philippine </span>
                                        
                                        <label class="label label-primary" style="margin-top:5px;font-size:12px"><i class="fa fa-clock-o"></i> <span id="clock_ph"> </span></label>
                                    </div>
                                </div>
                                <div class="col-md-3" style="display: inline-block">
                                    <div style="text-align: center" >
                                        <img src="{{ asset('images/usa.png') }}" alt="philippine time" width="20px" height="15px">
                                        <span>California </span>
                                        
                                        <label class="label label-danger" style="margin-top:2px;font-size:12px"><i class="fa fa-clock-o"></i> <span id="clock_ca"></span> </label>
                                    </div>
                                </div>
                                <div class="col-md-3" style="display: inline-block">
                                    <div style="text-align: center" >
                                            <img src="{{ asset('images/aus.png') }}" alt="philippine time" width="20px" height="15px">
                                            <span>Australia 
                                            <label class="label label-success" style="margin-top:2px;font-size:12px"><i class="fa fa-clock-o"> </i> <span id="clock_aus"></span>  </label>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>

              </div>
        
        </div>
    </div>
   
   @include("includes.footer") 
    
   @yield("custom_js")
</body>

</html>

