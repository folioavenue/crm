@extends('layouts.master')

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
         <div class="row">
            
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Ip Address Lists</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                                <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-permission" >
                            <thead>
                                <tr>
                                    <th>Ip Name</th>
                                    <th>Ip Address</th>
                                  
                                
                                </tr>
                            </thead>
                            <tbody>
                             @foreach($ipaddresses as $ipaddress)
                                <tr>
                                    <td style="color:#1ab394;"><a href="{{ url('ipaddress/'.$ipaddress->id.'/edit/')}}">{{ $ipaddress->name }}</a></td>
                                    <td>{{ $ipaddress->value }}</td>
                                  
                                </tr>
                             @endforeach   
                             
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Ip Name</th>
                                    <th>Ip Address</th>
                                    
                                </tr>
                            </tfoot>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
           
       </div>
    </div>
@endsection

@section('custom_js')

@endsection