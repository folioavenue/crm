@extends('layouts.master')

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
           <div class="col-lg-5">
               <div class="ibox float-e-margins">
                   <div class="ibox-title">
                       <h5>Enter Ip Address </h5>
                       <div class="ibox-tools">
                           <a class="collapse-link">
                               <i class="fa fa-chevron-up"></i>
                           </a>
                           <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                               <i class="fa fa-wrench"></i>
                           </a>
                          
                           <a class="close-link">
                               <i class="fa fa-times"></i>
                           </a>
                       </div>
                   </div>
                   <div class="ibox-content">
                       <div class="row">
                           <div class="col-sm-12">                                   
                                @if(isset($ipaddressdata))
                                    <form role="form" action="{{ url('ipaddress/'.$ipaddressdata->id) }}" method="POST">
                                    <input type="hidden" name="_method" value="PUT">
                                    @else
                                    <form role="form" action="{{ route('ipaddress.store') }}" method="POST">   
                                    @endif                            
                                    @csrf
                                    <div class="form-group"><label>IP Name</label> <input type="text" name="name" value="@if(isset($ipaddressdata)) {{ $ipaddressdata->name }} @endif" placeholder="Enter Ip Name" class="form-control" required></div>
                                    <div class="form-group"><label>IP Address</label> <input type="text" value="@if(isset($ipaddressdata)) {{ $ipaddressdata->value }} @endif" name="value" placeholder="Enter Ip Address" class="form-control" required> </div>
                                    
                                    <button class="ladda-button btn btn-primary pull-right" data-style="slide-right">@if(isset($ipaddressdata)) Update @else Save @endif</button>                               
                                  </form>          
                            </div>                                 
                      </div>
                           
                   </div>
               </div>
           </div>
           <div class="col-lg-7">
               <div class="ibox float-e-margins">
                   <div class="ibox-title">
                       <h5>Ip Address Lists</h5>
                       <div class="ibox-tools">
                           <a class="collapse-link">
                               <i class="fa fa-chevron-up"></i>
                           </a>
                           <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                               <i class="fa fa-wrench"></i>
                           </a>
                               <ul class="dropdown-menu dropdown-user">
                               <li><a href="#">Config option 1</a>
                               </li>
                               <li><a href="#">Config option 2</a>
                               </li>
                           </ul>
                           <a class="close-link">
                               <i class="fa fa-times"></i>
                           </a>
                       </div>
                   </div>
                   <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-permission" >
                                <thead>
                                    <tr>
                                        <th>IP Name</th>
                                        <th>IP Address</th>                                    
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($ipaddresses as $ipaddress)
                                    <tr>
                                        <td ><a href="{{ url('ipaddress/'.$ipaddress->id.'/edit') }}" style="color:#1ab394;">{{ $ipaddress->name }}</a></td>
                                        <td>{{ $ipaddress->value}}</td>
                                    </tr>
                                    @endforeach   
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>IP Name</th>
                                        <th>IP Address</th>                                        
                                    </tr>
                                </tfoot>
                            </table>
                       </div>
                   </div>
               </div>                
           </div>
        </div>
      </div>
  



@endsection