<?php

namespace App\Http\Middleware;

use Closure;
use App\IpAddress;
use Illuminate\Support\Facades\Auth;

class IpAddressChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ip_addresses = IpAddress::pluck('value')->toArray();
        
        if(Auth::check()){
            if(in_array(request()->ip(), $ip_addresses) || Auth::user()->hasRole(['superadmin','administrator','ip.administrator'])) {
                return $next($request);
            } else {
                die('invalid ip');
            }
        }else{
            return $next($request);
        }
    }
}
