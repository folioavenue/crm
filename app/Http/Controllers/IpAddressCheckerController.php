<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\IpAddress;
class IpAddressCheckerController extends Controller
{
    public function __construct()
    {
        view()->share(['page_title' => 'Ip Addresses']);
        $this->middleware('auth');
        $this->middleware(function ($request,$next){

            $this->ipaddresses = IpAddress::get();
        
            return $next($request);
         });      

    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        view()->share(['breadcrumb' => 'Ip Address','sub_breadcrumb'=> 'IP Address Lists']);

        return view('modules.ipaddress.index')
                ->with('ipaddresses',$this->ipaddresses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share(['breadcrumb' => 'Ip Address','sub_breadcrumb'=> 'Add New IP Address']);

        return view('modules.ipaddress.create')
                ->with('ipaddresses',$this->ipaddresses);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ip = IpAddress::create([
                        'name' => $request->name,
                        'value'=> $request->value,
                        ]);
        
        if($ip)
        {
            session()->flash('message','New ip address successfully added!');            
        }else{
            session()->flash('error_message','Fail to add ip address!');                        
        }          

        return redirect('ipaddress/create');
                             
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        view()->share(['breadcrumb' => 'Ip Address','sub_breadcrumb'=> 'Edit IP Address']);

        $ipaddressdata = IpAddress::find($id);
        
        return view('modules.ipaddress.create')
                  ->with('ipaddressdata',$ipaddressdata)
                ->with('ipaddresses',$this->ipaddresses);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ipaddress = IpAddress::find($id);

        $ipaddress->name = $request->name;
        $ipaddress->value = $request->value;

        if($ipaddress->save()){
            session()->flash('message','Ip address successfully updated!');            
        }else{
            session()->flash('error_message','Fail to update ip address!');                        
        }
        
        return redirect('ipaddress/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
